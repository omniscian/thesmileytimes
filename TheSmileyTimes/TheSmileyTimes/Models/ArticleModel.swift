//
//  ArticleModel.swift
//  TheSmileyTimes
//
//  Created by Ian Houghton on 23/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import Foundation

struct ListArticle {
    let title: String
    let source: String
    let contributor: String
    let url: String
    var imageUrl: String?
    
    init (articleDict: [String: AnyObject]){
        self.title = articleDict["data"]?["title"] as! String
        self.url = articleDict["data"]?["url"] as! String
        self.contributor = articleDict["data"]?["author"] as! String
        self.source = articleDict["data"]?["domain"] as! String
        
        if let preview = articleDict["data"]?["preview"] as? [String:AnyObject]{
            let imageArr = preview["images"] as! [[String:AnyObject]]
            let imageSource = imageArr[0]
            self.imageUrl = imageSource["source"]?["url"] as? String
        }
    }
}

struct ListArticleResponse{
    let articles: [ListArticle]
    let afterKey: String
    
    init (response: [String: AnyObject]){
        let articles = response["data"]?["children"] as! [[String:AnyObject]]
        let after = response["data"]?["after"] as! String
        
        var articleModels = [ListArticle]()
        for articleDict in articles{
            
            let article = ListArticle(articleDict: articleDict)
            articleModels.append(article)
        }
        
        self.articles = articleModels
        self.afterKey = after
    }
}
