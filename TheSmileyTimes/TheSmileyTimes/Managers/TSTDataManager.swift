//
//  TSTDataManager.swift
//  TheSmileyTimes
//
//  Created by Ian Houghton on 23/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import UIKit

class TSTDataManager: NSObject {
    
    private let baseUrlString = "https://www.reddit.com/r/UpliftingNews/.json?count=25"

    func getNews(start: String?, completion: @escaping (_: Bool, _ : ListArticleResponse?) -> Void){
        
        var urlSuffix = ""
        
        if (start != nil) {
            urlSuffix = "&after=" + start!
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: baseUrlString + urlSuffix)! as URL)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler:{
            data, response, err -> Void in
            
            if (err == nil){
                if let parsedData = try? JSONSerialization.jsonObject(with: data!) as! [String:AnyObject] {
                    
                    let articleResponse = ListArticleResponse(response: parsedData)
                    
                    DispatchQueue.main.async {
                        completion(true, articleResponse)
                    }
                }
            }
            else{
                DispatchQueue.main.async {
                    completion(true, nil)
                }
            }
        })
        
        task.resume()
    }
    
}
