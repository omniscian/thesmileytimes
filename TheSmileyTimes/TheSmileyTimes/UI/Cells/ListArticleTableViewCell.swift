//
//  ListArticleTableViewCell.swift
//  TheSmileyTimes
//
//  Created by Ian Houghton on 28/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import UIKit

class ListArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var mainImageView: UIImageView?
    @IBOutlet weak var sourceLabel: UILabel?
    @IBOutlet weak var contributorLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainImageView?.layer.cornerRadius = 3
        self.mainImageView?.clipsToBounds = true
        self.layoutIfNeeded()
    }
    
    func configureForListArticle(listArticle: ListArticle){
        self.titleLabel?.text = listArticle.title
        self.sourceLabel?.text = listArticle.source
        self.contributorLabel?.text = listArticle.contributor
        
        if (listArticle.imageUrl != nil) {
            self.mainImageView?.sd_setImage(with: NSURL(string: listArticle.imageUrl!) as URL!, placeholderImage: UIImage(named: "Placeholder"))
        }
        else{
            self.mainImageView?.image = UIImage(named: "Placeholder")
        }
    }

}
