//
//  FirstViewController.swift
//  TheSmileyTimes
//
//  Created by Ian Houghton on 23/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import UIKit

class StoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView : UITableView?
    
    let dataManager = TSTDataManager()
    let datasource : NSMutableArray = NSMutableArray.init()
    var refreshControl = UIRefreshControl()
    var paginationId : String?
    var selectedArticle : ListArticle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView?.estimatedRowHeight = 300.0
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        
        self.refresh()
        
        self.addPullToRefresh()
    }

    func addPullToRefresh(){
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(StoriesViewController.refresh), for: UIControlEvents.valueChanged)
        self.tableView?.addSubview(self.refreshControl)
    }

    func requestNews() -> Void {
        dataManager.getNews(start: self.paginationId, completion:{ (success, response) in
            if success{
                self.paginationId = response?.afterKey
                self.datasource.addObjects(from: (response?.articles)!)
                self.refreshControl.endRefreshing()
                self.tableView?.reloadData()
            }
        })
    }
    
    func refresh(){
        self.datasource.removeAllObjects()
        self.tableView?.reloadData()
        self.paginationId = nil
        self.requestNews()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StoriesToWebViewSegue" {
            let webViewController = segue.destination as! WebViewController
            webViewController.configureForUrl(url: (self.selectedArticle?.url)!)
        }
    }
    
    // MARK: UITableViewDatasource
    
    private func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.datasource.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell") as! ListArticleTableViewCell
        let listArticle = self.datasource[indexPath.row] as! ListArticle
            
        cell.configureForListArticle(listArticle: listArticle)
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView?.deselectRow(at: indexPath, animated: true)
        
        self.selectedArticle = self.datasource[indexPath.row] as? ListArticle
        
        self.performSegue(withIdentifier: "StoriesToWebViewSegue", sender: nil)
    }
    
    // MARK: UITableViewDelegate
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = self.datasource.count - 1
        if indexPath.row == lastElement {
            self.requestNews()
        }
    }
}

