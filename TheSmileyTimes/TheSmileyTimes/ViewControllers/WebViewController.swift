//
//  WebViewController.swift
//  TheSmileyTimes
//
//  Created by Ian Houghton on 27/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView : UIWebView?
    
    var url : String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.webView?.loadRequest(URLRequest(url: URL(string: url!)!))
        self.webView?.delegate = self
    }
    
    func configureForUrl(url: String) -> Void{
        self.url = url
    }
    
    override func webViewDidFinishLoad(_ webView: UIWebView) {
        let result = self.webView?.stringByEvaluatingJavaScript(from: "document.body.innerText")
        if let returnedString = result {
            println("the result is \(returnedString)")
        }
    }

}
